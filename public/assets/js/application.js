$(document).ready(function() {

          $.fn.dataTable.ext.search.push(
          function (settings, data, dataIndex) {
          var min = $('#min').datepicker("getDate");
          var max = $('#max').datepicker("getDate");
          var dateString = data[5].split("-");
          var startDate = new Date(data[5]);
          if (min == null && max == null) { return true; }
          if (min == null && startDate <= max) { return true;}
          if(max == null && startDate >= min) {return true;}
          if (startDate <= max && startDate >= min) { return true; }
          return false;
      }
      );

        $.fn.dataTableExt.oStdClasses.sSortDesc = "headerSortUp";
        $.fn.dataTableExt.oStdClasses.sSortAsc  = "headerSortDown";
        $.fn.dataTableExt.oStdClasses.sSortable  = "";


        $("#min").datepicker({ onSelect: function () { dta_table.draw(); }, changeMonth: true, changeYear: true });
        $("#max").datepicker({ onSelect: function () { dta_table.draw(); }, changeMonth: true, changeYear: true });  

          var currentDate = new Date();
          var day = currentDate.getDate();
          var month = currentDate.getMonth() + 1;
          var year = currentDate.getFullYear();

          var fileName = day + "-" + month + "-" + year;
        
          var dta_table = $('#dashboard').DataTable(
            {
              "iDisplayLength": 25,
              "columnDefs": [
                  {
                      "targets": [ 8,9,10,11,12,13,14,15,16,17,18,19,20 ],
                      "visible": false,
                      "searchable": false
                  }
              ],
              "order": [[ 5, "desc" ]],
              buttons: [
                  // 'csv', 'excel'
                  {
                      text: 'CSV <span class="icon icon-download"></span> ',
                      extend: 'csvHtml5',
                      filename: fileName,
                      extension : '.csv',
                      className: 'btn btn-primary-outline',
                      exportOptions: { }
                  },
                  {
                      text: 'Excel <span class="icon icon-download"></span> ',
                      extend: 'excelHtml5',
                      filename: fileName,
                      extension : '.xlsx',
                      className: 'btn btn-primary-outline',
                      exportOptions: { }
                  }
              ]
            }
          );

        dta_table.buttons().container()
          .appendTo( $('#downloads') );

        // Event listener to the two range filtering inputs to redraw on input
        $('#min, #max').change(function() {
            dta_table.draw();
        } );

        $('#status_filter').change( function () {
            dta_table
                .columns( 7 )
                .search( this.value )
                .draw();
        } );

        // Event listener to clear filter button
        $('#clear_filters').click(function() {
          $('#status_filter').val('').trigger('change');
          $('#min').datepicker('update', '');
          $('#max').datepicker('update', '');
            dta_table
             .search( '' )
             .columns().search( '' )
             .draw();
          $("[data-sort=table]").tablesorter({
          // Sort on the date column, in desc order
            sortReset   : true
        });
       });

        var leads_data_table = $('#lead_details').DataTable(
            {

              dom : 'B',
              "bSort" : false,
              buttons: [
                  // 'csv', 'excel'
                  {
                      text: 'CSV <span class="icon icon-download"></span> ',
                      extend: 'csvHtml5',
                      filename: fileName,
                      extension : '.csv',
                      className: 'btn btn-primary-outline',
                      exportOptions: { }
                  },
                  {
                      text: 'Excel <span class="icon icon-download"></span> ',
                      extend: 'excelHtml5',
                      filename: fileName,
                      extension : '.xlsx',
                      className: 'btn btn-primary-outline',
                      exportOptions: { }
                  }
              ]

            }
          );


          // dta_table.buttons().container()
          //   .appendTo( $('#lead_downloads') );


      } );