jQuery(function() {
    resizeDefaultCards();
});




function resizeDefaultCards() {
    jQuery('.card-default:not(.product-card)').each(function() {

        var card = jQuery(this);
        card.height(card.width() * 1.1);

    });

    jQuery('.card-default.product-card').each(function() {

        var card = jQuery(this);
        card.height(card.width() * 1.2);

        // Get the text height here
        var c_height = card.find('.description').height();
        var p_margins = parseInt(card.find('.description p').css('margin-top'));

        c_height = c_height + p_margins;

        card.find('.reveal-container').css('bottom', '-' + c_height + 'px');

    });
}

