
	// requires jQuery...

	var onloadFunctions = [];
	
	var ol = window.onload;
	window.onload = function(){
		
		if (typeof ol == 'function')
			ol();
		
		jQuery.each(onloadFunctions, function(i, func){
			func();
		});
		
	};
	
	function onloader(func)
	{
		onloadFunctions.push(func);
	}