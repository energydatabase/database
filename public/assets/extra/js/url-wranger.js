
	/*
		(c)2017 Cavalry Media
		www.hellocavalry.com
		
		URL Wrangler Script.
		Designed to assist with URL-related mangling.
		Required jQuery.
		
		Last updated: 06 July 2017
		
	*/
	
	// this function is somewhat universal...
	// pass a blank URL variable if you just want to use the current browser URL...
	function getQueryString(url)
	{
		var query_string = {};
		var query = '';
		if (url == '')
			query = window.location.search.substring(1);
		else
			query = (url.indexOf('?') != -1 ? url.substring(url.indexOf('?')+1) : '');
		
		var vars = query.split("&");
		for (var i=0;i<vars.length;i++)
		{
			var pair = vars[i].split("=");
			if (decodeURIComponent(pair[0]) != '')
				query_string[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
		}
		
		return query_string;
	}
	