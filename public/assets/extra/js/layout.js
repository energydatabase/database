onloader(function() {
    setTimeout(function(){
    	jQuery(window).trigger('resize');
    }, 500);
});

// resize function area...
// update the position of all of our middle vertically-aligned elements...
// resize default cards...
jQuery(window).on('resize.cavalry.com', function() {
    verticalAlignElementsUpdate();
    resizeDefaultCards();
});

jQuery(function() {

    // update any marker-enabled text...
    updateMarkerText();

    // update the same-height columns...
    updateSameHeightColumns();

    // update the password fields...
    jQuery('.password-field').each(function() {

        jQuery(this).on('focus.cavalry.com', function() {
            jQuery(this).prop('type', 'password');
        });

        jQuery(this).on('blur.cavalry.com', function() {
            if (jQuery(this).val() == '')
                jQuery(this).prop('type', 'text');
        });
    });

    // resize default cards...
    resizeDefaultCards();

    // Intialize Fancy Dropdowns
    $('select').cavalryDropdowns().init();

    // Collection of front end validators
    inputValidators();

    // Tooltip activators
    tooltipInit();

    // Navigation scroll listeners
    $.scrollDetection();

    // Quote path total sticky funcs
    stickyTotal();

    // Cookie checker
    cookiesDisclaimer();

    // This gives us fancy labels
    setTimeout(inputListeners, 1000);

    // Initialize datepicker
    datepickerInit();

    // inline editable fields listeners
    editableFields();

    // Internet Explorer 6-11 - Bool
    IEcheck();
    

    var base_path = base_path;


});

// ++++++++++++++++++++++++++++++++++++++++++++++++
// 
//          FRONTEND SPICE FUNCTIONS
// 
// ================================================

function IEcheck() {

    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    if(isIE) {
        $('body').toggleClass('internet-explorer');
    }

}

function inputValidators() {

    var phone_fields = $('input.phone-number')
    var datepicker_fields = $('input.datepicker');
    var max_length = 12;
    var timer;

    function datePickerPrevent() {
        datepicker_fields.on('keypress',function(e){
            e.preventDefault();
            return false;
        })
    };
    datePickerPrevent();

    function validatePhone(txtPhone) {

        var a = $(txtPhone);
        error_holder = '<div class="error-message"></div>';
        error_object = $('<div/>').html(error_holder).contents();

        clearTimeout(timer);
        timer = setTimeout(function() {
            var filter = /^[0-9-+]+$/;

            if(!checkUKTelephone(a.val())) {
                a.parent('.input-holder').toggleClass('error',false).toggleClass('error', true);
                if (!a.siblings('.error-message').length) {
                    error_object.html(telNumberErrors[telNumberErrorNo]).appendTo(a.parent());
                } else {
                    a.siblings('.error-message').html(telNumberErrors[telNumberErrorNo])
                }
            } else {

                setTimeout(function() {

                    error_object.remove();

                }, 500);
                a.parent('.input-holder').toggleClass('error', false);
            }
        }, 500);


    }

    phone_fields.on('keyup', function() {

        validatePhone($(this));

    }).on('keydown',function(){

        $(this).parent('.input-holder').toggleClass('error',false);

    });

}

function checkUKTelephone(telephoneNumber) {

    // Convert into a string and check that we were provided with something
    var telnum = telephoneNumber + " ";

    if(telnum.length < 10) {
        return false;
    }
    if (telnum.length == 1) {
        telNumberErrorNo = 1;
        return false
    }
    telnum.length = telnum.length - 1;

    // Don't allow country codes to be included (assumes a leading "+")
    var exp = /^(\+)[\s]*(.*)$/;
    if (exp.test(telnum) == true) {
        telNumberErrorNo = 2;
        return false;
    }

    // Remove spaces from the telephone number to help validation
    while (telnum.indexOf(" ") != -1) {
        telnum = telnum.slice(0, telnum.indexOf(" ")) + telnum.slice(telnum.indexOf(" ") + 1)
    }

    // Remove hyphens from the telephone number to help validation
    while (telnum.indexOf("-") != -1) {
        telnum = telnum.slice(0, telnum.indexOf("-")) + telnum.slice(telnum.indexOf("-") + 1)
    }

    // Now check that all the characters are digits
    exp = /^[0-9]{10,11}$/;
    if (exp.test(telnum) != true) {
        telNumberErrorNo = 3;
        return false;
    }

    // Now check that the first digit is 0
    exp = /^0[0-9]{9,10}$/;
    if (exp.test(telnum) != true) {
        telNumberErrorNo = 4;
        return false;
    }

    // Disallow numbers allocated for dramas.

    // Array holds the regular expressions for the drama telephone numbers
    var tnexp = new Array();
    tnexp.push(/^(0113|0114|0115|0116|0117|0118|0121|0131|0141|0151|0161)(4960)[0-9]{3}$/);
    tnexp.push(/^02079460[0-9]{3}$/);
    tnexp.push(/^01914980[0-9]{3}$/);
    tnexp.push(/^02890180[0-9]{3}$/);
    tnexp.push(/^02920180[0-9]{3}$/);
    tnexp.push(/^01632960[0-9]{3}$/);
    tnexp.push(/^07700900[0-9]{3}$/);
    tnexp.push(/^08081570[0-9]{3}$/);
    tnexp.push(/^09098790[0-9]{3}$/);
    tnexp.push(/^03069990[0-9]{3}$/);

    for (var i = 0; i < tnexp.length; i++) {
        if (tnexp[i].test(telnum)) {
            telNumberErrorNo = 5;
            return false;
        }
    }

    // Finally check that the telephone number is appropriate.
    exp = (/^(01|02|03|05|070|071|072|073|074|075|07624|077|078|079)[0-9]+$/);
    if (exp.test(telnum) != true) {
        telNumberErrorNo = 5;
        return false;
    }

    // Telephone number seems to be valid - return the stripped telehone number  
    return telnum;
}
var telNumberErrorNo = 0;
var telNumberErrors = new Array();
telNumberErrors[0] = "Valid UK telephone number";
telNumberErrors[1] = "Telephone number not provided";
telNumberErrors[2] = "UK telephone number without the country code, please";
telNumberErrors[3] = "UK telephone numbers should contain 10 or 11 digits";
telNumberErrors[4] = "The telephone number should start with a 0";
telNumberErrors[5] = "The telephone number is either invalid or inappropriate";

function inputListeners() {

    $('.input-holder.fancy-label-container').each(function(i, e) {

        el_type = $(e).find('input');
        var fancyLabel = $('<label class="fancy-label"></label>');

        //if ($(this).find('.fancy-label').length > 0)
        //    return false;
        
        if (el_type.length == 0) {

            //There is a select
            dropdownHolder = $(this).find('.fancy-dropdown-wrapper');
            if (dropdownHolder.length < 1)
            	return true;
            
            placeholder = dropdownHolder.siblings('select').data('placeholder');
            
            // Place placeholder into label
            fancyLabel.html(placeholder);

            // Place fancy label in input holder
            fancyLabel.prependTo($(this));
            
            jQuery(e).removeClass('fancy-label-container');


            // NB NB NB
            // 
            // Functions that control the placement of labels for dropdowns can be found in:
            // cavalryDropdowns();


        } else {

            // Normal input field
            inputHolder = el_type;

            // Fetch the name of the field
            // Create the attributes so the labels can target the correct input on focus
            inputID = inputHolder.prop('name');
            fancyLabel.prop('for', inputID + i);
            inputHolder.prop('id', inputID + i);
            placeholder = inputHolder.prop('placeholder');

            // Place placeholder into label
            fancyLabel.html(placeholder);

            // Place fancy label in input holder
            fancyLabel.prependTo($(this));

            inputHolder.removeAttr('placeholder');

            inputHolder.on('focus', function() {

                $(this).closest('.input-holder').toggleClass('focused', true);

            }).on('blur', function() {

                if ($(this).val() == '') {
                    $(this).closest('.input-holder').toggleClass('focused', false);
                };

            });
            
            jQuery(e).removeClass('fancy-label-container');

        }


    });

}

function editableFields() {


    if ($('.inline-editable').length) {


        var edit_fields = $('.inline-editable');

        $.each(edit_fields, function(index, val) {

            var input_holder = $(this).find('.field-input input');
            var select_holder = $(this).find('.field-input select');

            if (input_holder.length) {

                // Listeners for normal text fields
                input_holder.on('focus', function(e) {

                    parent = $(this).parent();

                    edit_fields.find('.editing').removeClass('editing');

                    if (parent.hasClass('editing')) {
                        // Make it non editable

                        parent.removeClass('editing');

                    } else {
                        // We would like to edit now

                        parent.addClass('editing');

                    }

                }).focusout(function(event) {

                    edit_fields.find('.editing').removeClass('editing');

                });

            } else {

                // Listeners for the fancy dropdown
                var dropdown = select_holder.siblings('.fancy-dropdown-wrapper');

                parent = $(this).parent();

                edit_fields.find('.editing').removeClass('editing');

                if (parent.hasClass('editing')) {
                    // Make it non editable

                    parent.removeClass('editing');

                } else {
                    // We would like to edit now

                    parent.addClass('editing');

                }

            };

        });
    }
}

function tooltipInit() {

    var opts = {
        placement: 'auto right',
        templates: '<div class="tooltip" role="tooltip"><div class="tooltip-inner"></div></div>'
    }

    $('[data-toggle="tooltip"]').tooltip(opts);

}

function resizeDefaultCards() {
    jQuery('.card-default:not(.product-card)').each(function() {

        var card = jQuery(this);
        card.height(card.width() * 1.1);

    });

    jQuery('.card-default.product-card').each(function() {

        var card = jQuery(this);
        card.height(card.width() * 1.2);

        // Get the text height here
        var c_height = card.find('.description').height();
        var p_margins = parseInt(card.find('.description p').css('margin-top'));

        c_height = c_height + p_margins;

        card.find('.reveal-container').css('bottom', '-' + c_height + 'px');

    });
}

function datepickerInit() {

    var datePicker_widths = new Array;

    // jQuery datepicker init and scripts
    $(".datepicker").each(function(index, el) {

        select_eq = index;

        $(this).datepicker({
            dateFormat: "yy-mm-dd",
            minDate: 0,
            startDate: 0,
            endDate: '+30d',
            maxDate: '+30d',
            beforeShow: function(ele, pickerInstance) {

                var w = $(ele).outerWidth();

                var x = $(ele).offset().top + 120;
                x += 250;
                x -= jQuery(window).height();

                if($(window).scrollTop() < x) {
                    $('html,body').animate({
                        scrollTop: x + 'px'
                    },450)
                }


                $('#ui-datepicker-div').insertAfter($(this));

                if (!datePicker_widths[select_eq] == w) {
                    datePicker_widths[select_eq].push(w);
                }

                datePicker_widths.push(w);
                picker = $(pickerInstance.dpDiv);
                picker.hide();

                setTimeout(function() {
                    picker.css('width', w);
                    picker.fadeIn(300);
                }, 1);

            },
            onSelect: function(dateText, inst) {
                if (inst.input.val() != '') {
                    inst.input.closest('.input-holder').toggleClass('focused', true);
                } else {
                    inst.input.closest('.input-holder').toggleClass('focused', false);
                }
            },
        });
    });
    datePickers = $('.datepicker-holder');

    if (datePickers.length) {

        var dateIconStruct = '<span class="date-icon"></span>';
        $.each(datePickers, function(index, val) {

            $(dateIconStruct).appendTo($(this));

            $(this).find('span.date-icon').on('click', function(e) {


                if (!$('#ui-datepicker-div').is(':visible')) {
                    $(this).siblings('.datepicker').focus();
                }

            })

        });

    }

}

function updateMarkerText() {
    jQuery('.marker-enabled').each(function() {

        var elem = jQuery(this);
        var pieces = elem.text().split(' ');
        var html = '';

        jQuery(pieces).each(function(i, p) {
            html += '<div class="marked-text">' + p + '</div>';
        });

        elem.html(html);

    });
}

function verticalAlignElementsUpdate() {
    jQuery('.vertical-align-middle').each(function() {

        var elem = jQuery(this);
        var parent = elem.parent();

        if (!elem.is(':visible'))
            elem.fadeIn(350);

        setTimeout(function(){
	        var top = (parent.height() / 2) - (elem.height() / 2);
	        elem.css({ 'top': top + 'px', 'position': 'relative' });
        }, 50);

    });
}

function updateSameHeightColumns() {
    if (jQuery(window).outerWidth(true) < 768) {
        return;
    }

    var available_width = jQuery(window).width();
    jQuery('.row.same-height-columns').each(function() {

        var max_height = 0;

        jQuery(this).find('> *').each(function(i, col) {
            col = jQuery(col);
            if (col.height() > max_height)
                max_height = col.height();
        });

        jQuery(this).find('> *').each(function(i, col) {
            col = jQuery(col);
            if (col.width() < (available_width * 0.9))
                col.height(max_height).css('position', 'relative');
        });

    });
}





//
// * * * * *
// hamburger menu...

var hamburger_semaphore = false;

function triggerHamburger() {
    if (!hamburger_semaphore) {
        hamburger_semaphore = true;
        var menu = jQuery('header .hamburger-menu');
        var navigation = jQuery('header .mobile-nav');
        var overflow_targets = $('html,body');

        if (menu.hasClass('open')) {
            // close it...
            menu.toggleClass('open', false);
            navigation.toggleClass('open', false);
            overflow_targets.toggleClass('open', false)
            overflow_targets.removeAttr('style');
        } else {
            // open it...
            menu.toggleClass('open', true);
            navigation.toggleClass('open', true);
            overflow_targets.toggleClass('open', true);
            overflow_targets.css('overflow', 'hidden');
        }

        hamburger_semaphore = false;
    }
}

$.scrollDetection = function(options) {

    var header = $('body > header');
    var st = $('.sticky-total');

    var settings = $.extend({
        scrollDown: function() {
            header.toggleClass('nav-hidden', true);
            if(st.length)
                st.css('top',0 + 'px');
        },
        scrollUp: function() {
            header.toggleClass('nav-hidden', false);
            if(st.length)
                st.css('top',70 + 'px');
        }
    }, options);

    var scrollPosition = 0;
    $(window).scroll(function (e) {

        var target = $(event.target);

        // Stop the scroll if its the homepage
        var homePage = $('.splash-screen-container');
        if(homePage.length) {
            return false;
        }

        if(!target.hasClass('fancy-dropdown') && target.parents('.fancy-dropdown').length == 0) {

            var cursorPosition = $(this).scrollTop();
            if (cursorPosition > scrollPosition) {
                settings.scrollDown();
            }
            else if (cursorPosition < scrollPosition) {
                settings.scrollUp();
            }
            scrollPosition = cursorPosition;

        }


        
    });
};

function stickyTotal() {

    // Does this sticky total exist?
    if ($('.sticky-total').length) {

        st = $('.sticky-total');
        st_height = st.height();
        sticky_offset = $('.sticky-total').offset().top;
        nav_header = $('body > header');
        animating = false;
        spacer = $('<div class="spacer mt-sm-30 mt-xs-15"></div>');
        spacer.insertBefore(st).hide();

        $(window).on('scroll', function(w) {

            window_offset = $(document).scrollTop();

            if (window_offset > sticky_offset) {

                // Show the sticky total
                st.toggleClass('fixed',true);
                spacer.height(st_height).show();

            } else {

                st.toggleClass('fixed',false);
                spacer.height(st_height).hide();

            }

        })

    }

}


//Click outside closes any open dropdowns
$(document).on('click', function(event) {
    if (!$(event.target).closest('.fancy-dropdown-wrapper').length) {


        $('.fancy-dropdown-wrapper').toggleClass('open', false);
        if ($('.fancy-dropdown').is(':visible')) {
            $('.fancy-dropdown').slideUp(200);

        }
        var parent = $('.fancy-dropdown').parentsUntil('.inline-editable');

        if (parent.length) {
            parent.removeClass('editing');
        }
    }
});


function cookiesDisclaimer() {

    var cookies_container = $('.cookies-disclaimer');

    if (!Cookies.get('cookies-accepted')) {
        $(cookies_container).css('bottom', '0');
        cookies_container.find('.close').on('click', function() {
            Cookies.set('cookies-accepted', 'yes', { expires: 31 });
            $(cookies_container).css('bottom', '-100%');
        })
    }

}
// Page loader holder
var pl;
var isAnimating = false;
var loaderTimer;
function pageLoader(toggle,text) {

    if (pl == undefined)
        pl = $('.page-loader');

    if(text != undefined) {
        pl.find('h4').text(text);
    } else {
        pl.find('h4').text('Loading');
    }

    if (toggle == true) {

        pl.stop(true, false).fadeIn(350);

    } else {

        clearTimeout(loaderTimer);
        loaderTimer = setTimeout(function(){
            pl.stop(true, false).fadeOut(450, function(){
            	jQuery(this).removeAttr('style');
            });    
        },251);
        

    }

}





// ++++++++++++++++++++++++++++++++++++++++++++++++
// 
//          BACKEND -> FRONTEND FUNCTIONS
// 
// ================================================




// All input fields must have a parent container.
// We use this to keep all the error message/fields together
function triggerFieldErrorMessage(selector, message) {

    // Fetch these variables for later
    target = $(selector);
    t_parent = target.closest('.input-holder');

    // Create the error message struct and convert to an object
    error_holder = '<div class="error-message"></div>';
    error_object = $('<div/>').html(error_holder).contents();

    // Add the error class
    t_parent.toggleClass('error', true);

    // Does the error message div exist?
    if (!target.siblings('.error-message').length) {

        error_object.html(message);
        error_object.appendTo(t_parent);

    } else {

        target.siblings('.error-message').remove();
        error_object.html(message);
        error_object.appendTo(t_parent);

    }


    // Remove the error message if the fields are focused
    if (target.length > 0) {
        tagType = target.get(0).tagName;

        if (tagType == 'SELECT') {

            target.siblings('.fancy-dropdown-wrapper').on('click', function(e) {

                $(this).closest('.input-holder').removeClass('error');
                $(this).closest('.error-message').fadeOut(300, function() {

                    $(this).remove();

                });

            });

        } else {

            target.on('click', function(e) {

                $(this).closest('.input-holder').removeClass('error');
                $(this).closest('.error-message').fadeOut(300, function() {

                    $(this).remove();

                });

            });

            target.on('focus', function(e) {

                $(this).closest('.input-holder').removeClass('error');
                $(this).closest('.error-message').fadeOut(300, function() {

                    $(this).remove();

                });

            });
        }
    }

    // Here we run a timeout to scroll to the first error message
    var timer;
    clearTimeout(timer);
    timer = setTimeout(function() {

        scrollToFirstError(target.closest(':not(.modal) form'));

    })


}
var errorTimer;
function scrollToFirstError(form) {

    clearTimeout(errorTimer);
    errorTimer = setTimeout(function(){

        var target = $(form).find('.error:first');

        if (target.length) {
            t_offset = target.offset().top - 120;

            $('html,body').animate({
                scrollTop: t_offset
            }, 500);
        }

    },100)

    

}

function triggerModalSuccess(btn, successMsg) {

    var timer;
    container = $('<div class="alert alert-success" style="display:none;margin-top:20px;margin-bottom:0;font-size:14px;"></div>');

    container.text(successMsg).insertAfter(btn).slideDown(350, function() {

        clearTimeout(timer);
        timer = setTimeout(function() {
            container.fadeOut(350, function() {
                $(this).remove();
            }).slideUp(200);
        }, 4000);

    });


}

function disableButton(selector, wait_msg) {

    $(selector).attr('disabled', true).text(wait_msg);

}

function enableButton(selector, wait_msg) {

    $(selector).attr('disabled', false).text(wait_msg);

}

function populateFields(step)
{
	if (global_ammoLocker.isUserAvailable())
	{
		jQuery('#recaptcha').hide();
		global_ammoLocker.addRequest('GET', 'get-user', {}, function(data){
			
			pageLoader(false);
		    
            // success...
            if (data.status == 200) {
                var response_object = JSON.parse(data.responseText);
                var user = JSON.parse(response_object.user.personal_details);
                var cover = JSON.parse(response_object.cover.data.data);
                
                jQuery.each(user, function(ele, val) {

                    // switch thru keys and give them nice headings...
                    var element = jQuery('[name="' + ele + '"]');

                    if (element.length > 0) {
                        var tagname = element.get(0).tagName.toLowerCase();

                        switch (tagname) {
                            case 'select':
                                if(val != '')
                                   jQuery('[name="' + ele + '"]').siblings('.fancy-dropdown-wrapper').find('#'+ ele +'_' + val + '').trigger('click');
                                break;

                            default:

                                switch (element.prop('type')) {
                                    case 'radio':
                                        element.prop('checked', false);
                                        jQuery('[name="' + ele + '"][value="' + val + '"]').prop('checked', true).trigger('click');
                                        break;

                                    default:
                                        jQuery('[name="' + ele + '"]').val(val).trigger('change');
                                        if (val != '')
                                            jQuery('[name="' + ele + '"]').closest('.input-holder').toggleClass('focused', true);
                                        break;
                                }

                            break;
                        }

                    }

                });

                jQuery.each(cover, function(section, variables) {

                    jQuery.each(variables, function(ele, val) {

                        // switch through keys and give them nice headings...
                        var element = jQuery('[name="' + ele + '"]');

                        if (element.length > 0) {
                            var tagname = element.get(0).tagName.toLowerCase();

                            switch (tagname)
                            {
                                case 'select':
                                    jQuery('[name="' + ele + '"]').siblings('.fancy-dropdown-wrapper').find('#'+ ele +'_' + val + '').trigger('click');
                                    break;

                                default:

                                    switch (element.prop('type')) {
                                        case 'radio':
                                            element.prop('checked', false);
                                            jQuery('[name="' + ele + '"][value="' + val + '"]').prop('checked', true).trigger('click');
                                            // This is specifically for the business quote step
                                            // It shows/hides the dropdown depending on value
                                            if(typeof showLiability === "function") {
                                                showLiability();
                                            }
                                            break;

                                        default:
                                            jQuery('[name="' + ele + '"]').val(val).trigger('change');

                                            if (val != '')
                                                jQuery('[name="' + ele + '"]').closest('.input-holder').toggleClass('focused', true);
                                            break;
                                    }

                                    break;
                            }

                        }

                    });

                });

            }
			
		});
	}
	else
        pageLoader(false);
	
}

var toast_timeout = 0;
function activateToastMessage(message)
{
	var toast = jQuery('<div class="toast-message"><div class="toast-message-inner">'+ message +'</div></div>');
	toast.hide();
	
	jQuery('.toast-message').remove();
	jQuery('body').append(toast);
	toast.fadeIn(350);
	
	clearTimeout(toast_timeout);
	toast_timeout = setTimeout(function(){
		jQuery('.toast-message').fadeOut(450, function(){
			jQuery(this).remove();
		});
	}, 4500);
}












