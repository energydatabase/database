
	$.fn.cavalryDropdowns = function(options) {
	
		var self = this;
	    this.cd_settings = $.extend({}, options);
	    
	    //
	    //
	    // initialisation function...
	    //
	    this.init = function() {
	    	
	    	jQuery.each(this, function(i, select){
	    		
	    		// check for the select element...
	    		if (!select.tagName.toLowerCase() == 'select')
	    			return true;
	    		
	    		if (jQuery(select).siblings('.fancy-dropdown-wrapper').length != 0)
	    			return true;
	    		
	    		var fancy_dropdown_struct = jQuery(
    				'<div class="fancy-dropdown-wrapper">' +
    					'<div class="fancy-selected" tabindex="0"></div>' +
    					'<div class="fancy-dropdown nano">' +
    						'<div class="nano-content"></div>' +
    					'</div>' +
    				'</div>'
	    		);
	    		
	    		jQuery(select).parent().append(fancy_dropdown_struct);
	    		jQuery(select).hide();
	    		
	    		// add in the fancy-selected click listener...
	    		fancy_dropdown_struct.find('.fancy-selected').on('click.fancyselected.cavalry.com', function() {
	    			
	    			var local_select = jQuery(this).parent().siblings('select:first');
	    			if (jQuery(this).parent().hasClass('open'))
	    				local_select.cavalryDropdowns().close();
	    			else
	    				local_select.cavalryDropdowns().open();

	    		});
	    		
	    	});
	
	        self.populateOptions();
	        
	        // *****
	        // HAMMOND PI CUSTOMISATION...
	        inputListeners();
	        // *****
	    };
	    
	    
	    //
	    //
	    // refresh / populate options...
	    //
	    this.populateOptions = function() {
	    	
	    	jQuery.each(this, function(i, select){
	    		
	    		// check for the select element...
	    		if (!select.tagName.toLowerCase() == 'select')
	    			return true;
	    		
	    		select = jQuery(select);
	    		var options = select.find('option');
	    		
	    		var fancy_wrapper = select.siblings('.fancy-dropdown-wrapper');
	    		var fancy_dropdown = fancy_wrapper.find('.fancy-dropdown');
	    		
	    		// this is used during nano-initialisation...
	    		fancy_wrapper.toggleClass('nano-initialised', false);
	    		
	    		if (fancy_dropdown.length == 1)
	    		{
	    			// remove any old options...
	    			fancy_dropdown.find('.option').remove();
	    			
	    			// (re)populate options...
	    			jQuery.each(options, function(j, option){
	    				
	    				option = jQuery(option);
	    				var option_struct = jQuery(
	    					'<div id="'+ select.prop('name') +'_'+ option.prop('value') +'" class="option" data-value="'+ option.prop('value') +'">' +
	    						option.text() +
	    					'</div>'
	    				);
	    				
	    				fancy_dropdown.find('.nano-content').append(option_struct);
	    				
	    				// add click listener...
	    				option_struct.on('click.fancydropdownoption.cavalry.com', function(){
	    					
	    					var option = jQuery(this);
	    					var option_value = option.data('value');
	    					
	    					var input_holder = option.parents('.input-holder:first');
	    					var fancy_dropdown_wrapper = option.parents('.fancy-dropdown-wrapper:first');
	    					var fancy_selected = fancy_dropdown_wrapper.find('.fancy-selected');
	    					var local_select = fancy_dropdown_wrapper.siblings('select');
	    					
	    					option.siblings().toggleClass('selected', false);
	    					option.toggleClass('selected', true);
	    					
	    					fancy_selected.html(option.text());
	    					
	    					local_select.val(option_value).trigger('click');
	    					
	    					jQuery(local_select).cavalryDropdowns().close();
	    					
	    					// activate focus states...
	    					if (input_holder.length > 0)
	    					{
	    						if (option_value != '')
	    							input_holder.toggleClass('focused', true);
	    						else
	    							input_holder.toggleClass('focused', false);
	    					}
	    					
	    				});
	    				
	    			});
	    		}
	    			
	    	});
	    	
	    };
	    
	    
	    //
	    //
	    // open dropdown function...
	    //
	    this.open = function() {
	    	
	    	// hide any other open dropdowns...
	    	jQuery('.fancy-dropdown:visible').slideUp(200)
	    		.parent().toggleClass('open', false);
	    	
	    	var local_select = jQuery(this);
	    	var fancy_dropdown_wrapper = local_select.siblings('.fancy-dropdown-wrapper');
	    	var fancy_dropdown = fancy_dropdown_wrapper.find('.fancy-dropdown');
	    	
	    	// update the height of the nano scroller and re-initialise in case of repopulate / resizing...
	    	if (!fancy_dropdown_wrapper.hasClass('nano-initialised'))
	    	{
	    		var height = 0;
		    	fancy_dropdown.find('.option').each(function(i, option){
		    		option = jQuery(option);
		    		height += option.outerHeight(true);
		    	});
		    	
		    	fancy_dropdown
		    		.css('height', height + 'px')
		    		.css('visibility', 'hidden')
		    		.show();
		    	
		    	fancy_dropdown.nanoScroller({ alwaysVisible: true, disableResize: true });
		    	
		    	fancy_dropdown
		    		.hide()
		    		.css('visibility', 'visible');
		    	
		    	fancy_dropdown_wrapper.addClass('nano-initialised');
	    	}
	    	
	    	fancy_dropdown_wrapper.toggleClass('open', true);
	    	fancy_dropdown.slideDown(400);
	    	
	    	// scroll to show the dropdown (if it's off the page)...
	        var x = fancy_dropdown_wrapper.offset().top + 370;
	        x -= jQuery(window).height();
	        
	        if (jQuery(window).scrollTop() < x)
	        	$('html,body').animate({scrollTop: x + 'px'}, 450);
	    };
	    
	    
	    //
	    //
	    // close dropdown function...
	    //
	    this.close = function() {

	        $(this).siblings('.fancy-dropdown-wrapper').toggleClass('open', false);
	        $(this).siblings('.fancy-dropdown-wrapper').find('.fancy-dropdown').slideUp(200);

	    };
	    
	    
	    //
	    //
	    // refresh function...
	    //
	    this.refresh = function() {

	        $(this).cavalryDropdowns().populateOptions();

	    };
	    
	    return this;
    
	};