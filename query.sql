-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.16 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Dumping data for table cerberus.monitoring_log_details: ~3 rows (approximately)
/*!40000 ALTER TABLE `monitoring_log_details` DISABLE KEYS */;
INSERT INTO `monitoring_log_details` (`id`, `client_ip`, `request_method`, `request_uri`, `response_size`, `response_time`, `request_status`, `request_time`, `timestamp`) VALUES
	(1, '127.0.0.1', 'GET', '/', '381', '103492', '401', '0', '1501151653'),
	(2, '127.0.0.1', 'GET', '/', '381', '103492', '401', '0', '1501151653'),
	(3, '127.0.0.1', 'GET', '/', '381', '103492', '401', '0', '1501151653');
/*!40000 ALTER TABLE `monitoring_log_details` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
