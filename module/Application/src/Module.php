<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\MvcEvent;

class Module
{
    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    private $config;
    private $serviceManager;

    public function onBootstrap(MvcEvent $event)
    {
        $application = $event->getApplication();
        $serviceManager = $application->getServiceManager();
        $this->serviceManager = $serviceManager;
    } 

    public function getServiceConfig()
    {
        return [
            'factories' => [
            
                'Demographics' => function($container) {
                    $tableGateway = $container->get(Model\Store\Demographics::class);
                    return new Model\DemographicsManager($tableGateway);
                },

                'Infrastructure' => function($container) {
                    $tableGateway = $container->get(Model\Store\Infrastructure::class);
                    return new Model\InfrastructureManager($tableGateway);
                },

                'Resources' => function($container) {
                    $tableGateway = $container->get(Model\Store\Resources::class);
                    return new Model\ResourcesManager($tableGateway);
                },

                'Projects' => function($container) {
                    $tableGateway = $container->get(Model\Store\Projects::class);
                    return new Model\ProjectsManager($tableGateway);
                },

                'States' => function($container) {
                    $tableGateway = $container->get(Model\Store\States::class);
                    return new Model\StatesManager($tableGateway);
                },

                Model\Store\Demographics::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Store\Demographics());
                    return new TableGateway('demographics', $dbAdapter, null, $resultSetPrototype);
                },

                Model\Store\Infrastructure::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Store\Infrastructure());
                    return new TableGateway('infrastructure', $dbAdapter, null, $resultSetPrototype);
                },

                Model\Store\Resources::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Store\Resources());
                    return new TableGateway('resources', $dbAdapter, null, $resultSetPrototype);
                },

                Model\Store\Projects::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Store\Projects());
                    return new TableGateway('projects', $dbAdapter, null, $resultSetPrototype);
                },

                Model\Store\States::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Store\States());
                    return new TableGateway('states', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }

    public function getControllerConfig()
    {
        $this->config = $this->getConfig();

        return [
            'factories' => [
                Controller\IndexController::class => function() {
                    return new Controller\IndexController($this->config, $this->serviceManager);
                },

                Controller\AdminController::class => function($container) {
                    return new Controller\AdminController(
                        $this->config, $container
                    );
                },
            ],
        ];
    }
}
