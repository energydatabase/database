<?php
/**
* Data Storage Application
* @todo Need to add some more functionality within functions
*
* @author Brian Nyatsine <brian@hellocavalry.com>
*/

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\ServiceManager\ServiceManager;
use Zend\Authentication\AuthenticationService;
use Application\Service\ShapeFileImporter;
use Application\Model\GeoJsonManager;

class IndexController extends AbstractActionController
{
    private $config;
    private $sm;

    public function __construct(array $config, ServiceManager $sm)
    {
        $this->config = $config;
        $this->sm = $sm;
    }

    public function indexAction()
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/main_layout');

        $view = new ViewModel(array(
            'data' =>  array(),
        ));

        // $view->setTemplate('admin/login');

        return $view;

    }

    public function geojsonAction()
    {
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Access-Control-Allow-Origin', '*');
        $response->getHeaders()->addHeaderLine('Access-Control-Allow-Methods', 'POST PUT DELETE GET');
        
        $matches = $this->getEvent()->getRouteMatch();
        $action = trim($matches->getParam('type', ''));

        $geojson = new GeoJsonManager($this->sm);
        $result = $geojson->getGeoJsonForMap($action);
        

        echo json_encode($result);
        return $response;
    }

    public function geoAction()
    {
        $shape_file = new ShapeFileImporter($this->sm);
        $shape_file->importFile('f');
    }


    public function updatefilesAction()
    {
        $resources =  array(
            'roads',
            'mines',
            'existing_mini_grids',
            'potential_mini_grids',
            'potential_shs_locations',
            'existing_shs_locations',
            'existing_transmission_lines',
            'existing_gas_lines',
            'power_plant_locations',
            'states_settlement_data',
            'health_care_centers',
            'school_locations',
            'water_access'
        );

        foreach ($resources as  $resource) {
            # code...
            $geojson = new GeoJsonManager($this->sm);
            $result = $geojson->getGeoJsonForMap($resource);
        }

        echo json_encode(array('result' => 'success'));

        $response = $this->getResponse();
        return $response;

    }


    public function registerAction()
    {

    }

    private function isUserAuthenticated() {
        //first we get the users details
        return true;
        $auth_service = new AuthenticationService();
        if ($auth_service->hasIdentity())
        {
            $identity = $auth_service->getIdentity();
            if (isset($identity['id']) && $identity['id'] != "")
            {
                return true;
            }

            return false;
        }

        return false;
    }
}
