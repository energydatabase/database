<?php

	/**
	 * This is the sitemap confirguration. It will map out all endpoints we'd
	 * like to monitor. To add another point of monitoring, simply add it to this
	 * file and wait for the system to refresh the cached resources in the:
	 * '/config/sitemap-resources' folder.
	 * 
	 * All items included here will be crawled for additional page-level elements, such as images,
	 * JavaScript, and CSS files (which should not specifically be included here).
	 * 
	 * Root-level items will be considered the main sites that we're looking to monitor, and
	 * each level requires the following format (root-level or child-level):
	 * 
	 * 'item_key' =>
	 *     'label'          => A label for this item.
	 *     'tags'           => An array of user-defined tags, useful for segregating sections of stats in the reports.
	 *     'endpoints'      => An array of string endpoints to hunt down in the reports table. Wildcards are acceptable, e.g.: "/index.php*"
	 *     'crawl_endpoint' => A string value containing the main endpoint the system can use for additional resource crawling.
	 *     'tablename'      => The string value containing the name of the table from which we can find our statistics (root-level item only).
	 *     'basepath'       => The string value of the site's base path (root-level item only).
	 *     'children'       => An array of items following this specified format, the children of the current item.
	 */

	return array(
		'production_1' => array(
			'label'		     => 'VMSA Production 1',
			'tags'		     => array(),
			'endpoints'	     => array('', '/', '/index.php'),
			'crawl_endpoint' => '/index.php',
			'tablename'	     => 'vmsa_prod_1',
			'basepath'       => 'https://www.virginmoneyinsurance.co.za',
			'children'	     => array(
				
				'car_insurance_main' => array(
					'label'		     => 'Car Insurance - Homepage',
					'tags'		     => array('auto'),
					'endpoints'	     => array('/car-insurance', '/car-insurance#*'),
					'crawl_endpoint' => '/car-insurance',
					'children'	     => array(
						'quote_motor' => array(
							'label'		     => 'Quote Auto Pages',
							'tags'		     => array('auto'),
							'endpoints'	     => array('/quote/motor', '/quote/motor#*'),
							'crawl_endpoint' => '/quote/motor',
							'children'	     => array(
									'index' => array(
										'label'		     => 'Quote Auto - Index',
										'tags'		     => array('auto'),
										'endpoints'	     => array('/quote/motor', '/quote/motor#*'),
										'crawl_endpoint' => '/quote/motor/motor',
										'children'	     => array()
									),

									'driver-details' => array(
										'label'		     => 'Quote Auto - Driver Details',
										'tags'		     => array('auto'),
										'endpoints'	     => array('/quote/driver-details', '/quote/driver-details*'),
										'crawl_endpoint' => '/quote/motor/driver-details',
										'children'	     => array()
									),

									'add-car' => array(
										'label'		     => 'Quote Auto - Add Car',
										'tags'		     => array('auto'),
										'endpoints'	     => array('/quote/add-car', '/quote/add-car?*'),
										'crawl_endpoint' => '/quote/motor/add-car',
										'children'	     => array()
									),

									'car-security-details' => array(
										'label'		     => 'Quote Auto - Car Security Details',
										'tags'		     => array('auto'),
										'endpoints'	     => array('/quote/car-security-details', '/quote/car-security-details?*'),
										'crawl_endpoint' => '/quote/motor/car-security-details',
										'children'	     => array()
									),

									'quote' => array(
										'label'		     => 'Quote Auto - Quote',
										'tags'		     => array('auto'),
										'endpoints'	     => array('/quote/quote', '/quote/quote#*'),
										'crawl_endpoint' => '/quote/motor/quote',
										'children'	     => array()
									),

									'customise-your-cover' => array(
										'label'		     => 'Quote Auto - customise-your-cover',
										'tags'		     => array('auto'),
										'endpoints'	     => array('/quote/motor/customise-your-cover', '/quote/motor/customise-your-cover?*'),
										'crawl_endpoint' => '/quote/motor/customise-your-cover',
										'children'	     => array()
									),

									'index' => array(
										'label'		     => 'Quote Auto - quote-summary',
										'tags'		     => array('auto'),
										'endpoints'	     => array('/quote/motor/quote-summary', '/quote/motor/quote-summary?*'),
										'crawl_endpoint' => '/quote/motor/quote-summary',
										'children'	     => array()
									),
									// adding in the ajax page here
									'get-quote' => array(
										'label'		     => 'Quote Auto - get-quote',
										'tags'		     => array('auto'),
										'endpoints'	     => array('/quote/motor/get-quote', '/quote/motor/get-quote?*'),
										'crawl_endpoint' => '/quote/motor/get-quote',
										'children'	     => array()
									)
								)
						)
					)
				),
					
				'home_insurance_main' => array(
					'label'		     => 'Home Insurance - Homepage',
					'tags'		     => array('home'),
					'endpoints'	     => array('/home-insurance', '/home-insurance#*'),
					'crawl_endpoint' => '/home-insurance',
					'children'	     => array(
						'quote_home' => array(
							'label'		     => 'Quote Home Pages',
							'tags'		     => array('home'),
							'endpoints'	     => array('/quote/home', '/quote/home#*'),
							'crawl_endpoint' => '/quote/home',
							'children'	     => array(
								'index' => array(
									'label'		     => 'Quote Home - Index',
									'tags'		     => array('auto'),
									'endpoints'	     => array('/quote/home', '/quote/home#*'),
									'crawl_endpoint' => '/quote/home',
									'children'	     => array()
								),

								'property-details' => array(
									'label'		     => 'Quote Home - property details',
									'tags'		     => array('auto'),
									'endpoints'	     => array('/quote/home/property-details', '/quote/home/property-details?*'),
									'crawl_endpoint' => '/quote/home/property-details',
									'children'	     => array()
								),

								'additional-property-details' => array(
									'label'		     => 'Quote Home - additional-property-details',
									'tags'		     => array('auto'),
									'endpoints'	     => array('/quote/home/additional-property-details', '/quote/home/additional-property-details?*'),
									'crawl_endpoint' => '/quote/home/additional-property-details',
									'children'	     => array()
								),

								'quote' => array(
									'label'		     => 'Quote Home - quote',
									'tags'		     => array('auto'),
									'endpoints'	     => array('/quote/home', '/quote/home/quote?*'),
									'crawl_endpoint' => '/quote/home/quote',
									'children'	     => array()
								),

								'index' => array(
									'label'		     => 'Quote Home Index',
									'tags'		     => array('auto'),
									'endpoints'	     => array('/quote/home', '/quote/home#*'),
									'crawl_endpoint' => '/quote/home',
									'children'	     => array()
								)
							)
						)
					)
				)
					
			),
		)
	);