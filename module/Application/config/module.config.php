<?php
namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'admin' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/admin[/:action[/:resource[/:id]]]',
                    'defaults' => [
                        'controller' => Controller\AdminController::class,
                        'action'     => 'index',
                        'resource'     => 'undefined',
                        'id'     => 0,
                    ],
                ],
            ],


            'app' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/home[/:action[/:type]]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                        'type'       => '',
                    ],
                ],
            ],

            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            // add more routes here
        ],
    ],
    'submission_config' => [
        'register_admin'  => array(
            'form'       => '\Application\Form\Nigeria\RegisterUserForm',
            'workers'       => array(
                                //Custom workers
                                '\Application\Workers\Nigeria\UserExistanceCheckWorker',
                                '\Application\Workers\Nigeria\RegisterAdminUserWorker'
                                //System workers
                            ),
            'requires_auth' => false
        ),
        'login_admin'  => array(
            'form'       => '\Application\Form\Nigeria\AdminLoginForm',
            'workers'       => array(
                                //Custom workers
                                 '\Admin\Workers\Nigeria\AdminLoginWorker',
                                //System workers
                            ),
            'requires_auth' => false
        ),
        'dashboard'  => array(
            'form'       => '',
            'workers'       => array(
                                //Custom workers
                                 '\Application\Workers\Nigeria\AdminDashboardWorker'
                                //System workers
                            ),
            'requires_auth' => false
        ),
        'detail'  => array(
            'form'       => '',
            'workers'       => array(
                                //Custom workers
                                 '\Application\Workers\Nigeria\DetailsWorker',
                                //System workers
                            ),
            'requires_auth' => false
        ),
        'reports'  => array(
            'form'       => '',
            'workers'       => array(
                                //Custom workers
                                 '\Application\Workers\Nigeria\ReportingWorker',
                                //System workers
                            ),
            'requires_auth' => false
        ),
        'admin_settings' => array(
            'form'          => '',
            'workers'       => array('\Application\Workers\Nigeria\AdminSettingsWorker'),
            'requires_auth' => false
        ),
        'update_settings' => array(
            'form'          => '',
            'workers'       => array('\Application\Workers\Nigeria\UpdateAdminSettingsWorker'),
            'requires_auth' => false
        )
           
    ],
    'view_helpers' => [
        'factories' => [
            View\Helper\Currentform::class => InvokableFactory::class,                    
        ],
       'aliases' => [
            'currentForm' => View\Helper\Currentform::class
       ]
    ], 
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'admin/admin/index' => __DIR__ . '/../view/admin/admin/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];